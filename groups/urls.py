"""upshake URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from groups import views
from django.views.static import serve

urlpatterns = [
	url(r'^$',views.group_list,name='group_list'),
    url(r'^create_group/(?P<user_id>[0-9]+)/$', views.create_group, name='create_group'),
    url(r'^add_member/(?P<group_id>[0-9]+)/$', views.add_member, name='add_member'),
    url(r'^messages/(?P<group_id>[0-9]+)/$', views.messages, name='messages'),
    url(r'^add_member_to_group/(?P<member_id>[0-9]+)/$', views.add_member_to_group, name='add_member_to_group'),
    url('transmit_msg',views.transmit_msg,name='transmit_msg'),
    url('monitor_new_msg',views.monitor_new_msg,name='monitor_new_msg'),

]

# for turning on media file access
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)