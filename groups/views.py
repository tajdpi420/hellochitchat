from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from friends.models import Friendship,Profile
from django.shortcuts import render
import time
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.db.models import Q
import os
from django.conf import settings
from django.http import JsonResponse
import json
from django.contrib.auth.models import User
from .models import Group,GroupMember,GroupMessage,MessageStatus
from django.core.serializers.json import DjangoJSONEncoder

# Create your views here.
@login_required
def group_list(request):
	group_list=Group.objects.filter(created_by=request.user)
	meAsMemberInGroups=GroupMember.objects.filter(member_id=request.user.id)
	return render(request,'group_list.html',{'group_list':group_list,'meAsMemberInGroups':meAsMemberInGroups})



@login_required
@csrf_exempt
def create_group(request,user_id):
	if request.method=='POST':
		group_json =json.loads(request.body)
		newGroup=Group(created_by_id=request.user.id,name=group_json['group_name'])
		newGroup.save()
		if newGroup.pk:
			return HttpResponse(json.dumps({"status": '200','group_id':str(newGroup.pk)}),content_type='application/json')
		else:
			return HttpResponse(json.dumps({"status": '404'}),content_type='application/json')
	else:
		pass


@login_required
@csrf_exempt
def add_member_to_group(request,member_id):
	if request.method=='POST':
		group_json =json.loads(request.body)

		newMember=GroupMember(group_id=group_json['group_id'],member_id=member_id)
		newMember.save()
		if newMember.pk:
			return HttpResponse(json.dumps({"status": '200','member_id':str(newMember.pk)}),content_type='application/json')
		else:
			return HttpResponse(json.dumps({"status": '404'}),content_type='application/json')
	else:
		pass

# def has_friendship(user_id_1,user_id_2):
# 	# print(user_id_1)
# 	# print(user_id_2)
# 	friendsip = Friendship.objects.filter(user_1_id=user_id_1,user_2_id=user_id_2)
# 	print(friendsip.count())
# 	if friendsip.count()>0:
# 		return True
# 	else:
# 		return False

@login_required
def add_member(request,group_id):
	group=Group.objects.get(id=group_id)
	friend_list=Friendship.objects.filter(user_1=request.user)
	return render(request,'add_member.html',{'group':group,'friend_list':friend_list})

@login_required
def messages(request,group_id):
	group=Group.objects.get(id=group_id)
	return render(request,'group_chat.html',{'group':group})

@login_required
@csrf_exempt
def transmit_msg(request):
	if request.method=='POST':
		msg =json.loads(request.body)
		newMsg=GroupMessage(group_id=msg['group_id'],sender_id=request.user.id,message=msg['msg'],message_type="TEXT")
		newMsg.save()
		if newMsg.pk:
			statusOfMessageOfSender=MessageStatus(group_id=newMsg.group_id,receiver_id=request.user.id,status=1,group_message_id=newMsg.pk)
			statusOfMessageOfSender.save();
			updateMessageStatus(newMsg,request.user.id)
			return HttpResponse(json.dumps({"status": '200'}),content_type='application/json')
		else:
			return HttpResponse(json.dumps({"status": '404'}),content_type='application/json')
	else:
		pass

def updateMessageStatus(message,user_id):
	members=GroupMember.objects.filter(group_id=message.group_id).exclude(member_id=user_id)
	for member in members:
		print(member.member.username)
		print("------------------------")
		newStatus=MessageStatus(group_id=member.group_id,receiver_id=member.member_id,status=0,group_message_id=message.id)
		newStatus.save()

@login_required
@csrf_exempt
def monitor_new_msg(request):
	print(str(request.POST.items()))
	if request.method=='POST':
		group_json =json.loads(request.body)
		newMsgs=MessageStatus.objects.filter(group_id=group_json['group_id'],receiver_id=request.user.id,status=0).order_by('id')
		tempMesg=list()
		for message in newMsgs:
			message.status=1
			message.save()
			print(message.group_message.message)
			print("------------------------")
			tempMesg.append({'msg':message.group_message.message,'username':message.group_message.sender.username,'img':message.group_message.sender.profile.profile_pic.url})
		return HttpResponse(json.dumps({"status": '200','messages':tempMesg},sort_keys=True,indent=1,cls=DjangoJSONEncoder),content_type='application/json')
	else:
		return HttpResponse(json.dumps({"status": '505'}),content_type='application/json')
		