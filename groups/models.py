# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe

from django.utils.timezone import get_current_timezone
from datetime import datetime

class Group(models.Model):
	created_by = models.ForeignKey(User, on_delete=models.CASCADE,related_name='owner')
	name = models.TextField(default="")
	status = models.IntegerField(default=1)# 1= active,0=inactive
	created_at = models.DateTimeField(auto_now=True)
	
	def __str__(self):
		return self.name
	
	def save(self, *args, **kwargs):
		self.created_at = datetime.now().replace(tzinfo=get_current_timezone())
		super(Group, self).save(*args, **kwargs)

class GroupMember(models.Model):
	group = models.ForeignKey(Group, on_delete=models.CASCADE,related_name='group')
	member = models.ForeignKey(User, on_delete=models.CASCADE,related_name='groupmember')
	status = models.IntegerField(default=1)# 1= active,0=inactive
	created_at = models.DateTimeField(auto_now=True)
	
	def __str__(self):
		return self.member.user.username

class GroupMessage(models.Model):
	group = models.ForeignKey(Group, on_delete=models.CASCADE,related_name='messaginggroup')
	sender = models.ForeignKey(User, on_delete=models.CASCADE,related_name='groupmessagesender')
	message = models.TextField(default="")
	message_type = models.TextField(default="TEXT")
	status = models.IntegerField(default=1)# 1= active,0=inactive
	created_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.message

class MessageStatus(models.Model):
	group = models.ForeignKey(Group, on_delete=models.CASCADE,related_name='status_of_group')
	receiver = models.ForeignKey(User, on_delete=models.CASCADE,related_name='receiver_status')
	group_message = models.ForeignKey(GroupMessage, on_delete=models.CASCADE,related_name='group_message')
	status = models.IntegerField(default=0)# 1= active,0=inactive
	created_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.status