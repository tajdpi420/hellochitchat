from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
# from .models import Album,Photo
from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
import time
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.db.models import Q
import os
from django.conf import settings
from django.http import JsonResponse
import json
from django.contrib.auth.models import User
from friends.models import Friendship,Profile
from friends.form import ImageUploadForm

def index(request):
	return render(request,'index.html')

# Create your views here.
def signup(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data['username']
			password = form.cleaned_data['password1']
			user = authenticate(username=username, password=password)
			profile = Profile(user=user,is_online=True)
			profile.save()
			login(request, user)
			return redirect('/friends/')
	else:
		form = UserCreationForm()
	return render(request, 'registration/signup.html', {'form': form})
@login_required
def profile(request):
	if request.method=='GET':
		print(request.user.id)
		return render(request, 'profile.html', {'user': User.objects.get(id=request.user.id)})
	elif request.method=='POST':
		print("Update called")
		form = ImageUploadForm(request.POST, request.FILES)
		first_name = request.POST['first_name']
		last_name = request.POST['last_name']
		email = request.POST['email']
		user = User.objects.get(id=request.user.id)
		user.first_name=first_name
		user.last_name=last_name
		user.email=email
		if form.is_valid():
			user.profile.profile_pic = form.cleaned_data['image']
			user.profile.save()
		user.save()

		print(user)
		return render(request, 'profile.html', {'user': User.objects.get(id=request.user.id),'msg':"Profile updated!"})
	else:
		pass
	