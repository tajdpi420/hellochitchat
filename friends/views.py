from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from .models import Friendship,Profile
from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
import time
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.db.models import Q
import os
from django.conf import settings
from django.http import JsonResponse
import json
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.dispatch import receiver  

@login_required
def list(request):
	friend_list=Friendship.objects.filter(user_1=request.user)
	return render(request,'friend_list.html',{'friend_list':friend_list})

@login_required
@csrf_exempt
def search(request):
	if request.method=='GET':
		generalUsers = User.objects.filter(is_superuser=False)
		generalUsers=generalUsers.exclude(id=request.user.id).order_by('-id')[0:5]
		return render(request, 'search_friend.html',{'users':generalUsers,'search_keyword':"",'title':"Recent users"})
	elif request.method=='POST':
		search_keyword = request.POST['search_keyword']
		generalUsers = User.objects.filter(is_superuser=False)
		generalUsers=generalUsers.filter(Q(username__contains=search_keyword)|Q(email__contains=search_keyword)|Q(first_name__contains=search_keyword) | Q(last_name__contains=search_keyword)).exclude(id=request.user.id)
		print(generalUsers.count())
		return render(request, 'search_friend.html',{'users':generalUsers,'search_keyword':search_keyword,'title':"Users found"})
		
	else:
		pass
	# generalUsers = User.objects.filter(is_superuser=False)
	# return render(request, 'search_friend.html')
@login_required
@csrf_exempt
def add_friend(request,user_id):
	user_2=User.objects.get(id=user_id)
	friendships=Friendship.objects.filter(user_1=request.user,user_2=user_2)
	print(friendships.count())
	if friendships.count()==0:
		newfriOBJ=Friendship(user_1=request.user,user_2=user_2)
		newfriOBJ.save()
		if newfriOBJ.pk:
			return HttpResponse(json.dumps({"status": '200'}),content_type='application/json')
		else:
			return HttpResponse(json.dumps({"status": '404'}),content_type='application/json')
	else:
		return HttpResponse(json.dumps({"status": '222'}),content_type='application/json')

@receiver(user_logged_in)
def got_online(sender, user, request, **kwargs):
	# profile, created = Profile.objects.get(user=user,is_online=True)
	user.profile.is_online = True
	user.profile.save()
@receiver(user_logged_out)
def got_offline(sender, user, request, **kwargs):
	# profile = Profile.objects.get(user_id=user.id)
	user.profile.is_online = False
	user.profile.save()


