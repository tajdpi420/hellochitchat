# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
# Create your models here.

class Friendship(models.Model):
	user_1 = models.ForeignKey(User, on_delete=models.CASCADE,related_name='user_1')
	user_2 = models.ForeignKey(User, on_delete=models.CASCADE,related_name='user_2')
	status = models.IntegerField(default=1)# 1= friends,0=unfriend,2=block
	created_at = models.DateField(auto_now=True)

	def __str__(self):
		return self.user_1.username+" is friends of "+user_2.username

class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile',on_delete=models.CASCADE)           
    is_online = models.BooleanField(default=False)
    profile_pic = models.ImageField(upload_to = 'users_pic/', default = 'users_pic/user.png')
    
    def __str__(self):
    	return self.user.username

