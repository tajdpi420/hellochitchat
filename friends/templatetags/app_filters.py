from django import template
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
register = template.Library()
from ..models import Friendship
from groups.models import GroupMember

@register.filter(name='is_already_member')
def is_already_member(group_id,user_id):
	# print(user_id_1)
	# print(user_id_2)
	try:
		membership=GroupMember.objects.filter(group_id=group_id,member_id=user_id)
		if membership.count()>0:
			print("membership found")
			return True
		else:
			return False
	except Exception:
		return False
# @register.filter(name='has_friendship')
# def has_friendship(user_id_1,user_id_2):
# 	print(user_id_1)
# 	print(user_id_2)
# 	friendsip = Friendship.objects.filter(user_1_id=user_id_1,user_2_id=user_id_2)
# 	print(friendsip.count())
# 	if friendsip.count()==0:
# 		return "Add friend"
# 	else:
# 		return "Already friend"

@register.filter(name='has_friendship')
def has_friendship(user_id_1,user_id_2):
	# print(user_id_1)
	# print(user_id_2)
	friendsip = Friendship.objects.filter(user_1_id=user_id_1,user_2_id=user_id_2)
	print(friendsip.count())
	if friendsip.count()>0:
		return True
	else:
		return False

@register.filter(name='logged_user_is_sender')
def logged_user_is_sender(user_id_1,user_id_2):
	# print(user_id_1)
	# print(user_id_2)
	if user_id_1==user_id_2:
		return True
	else:
		return False

# @register.filter(name='laod_friendship_class')
# def laod_friendship_class(user_id_1,user_id_2):
# 	# print(user_id_1)
# 	# print(user_id_2)
# 	friendsip = Friendship.objects.filter(user_1_id=user_id_1,user_2_id=user_id_2)
# 	print(friendsip.count())
# 	if friendsip.count()==0:
# 		return "btn-primary"
# 	else:
# 		return "btn-secondary disabled"

#
#
# @register.filter(name='get_challenge_info')
# def get_challenge_info(challengeID):
#     challenge = Challenge.objects.get(id=challengeID)
#     return challenge.challengeTitle
#
#
# @register.filter(name='get_challenge_deadline')
# def get_challenge_deadline(challengeID):
#     challenge = Challenge.objects.get(id=challengeID)
#     return challenge.challengeDeadline
