from django import forms
from .models import Friendship,Profile

class ImageUploadForm(forms.Form):
    """Image upload form."""
    image = forms.ImageField()