# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from django.utils.timezone import get_current_timezone
from datetime import datetime

class Message(models.Model):
	from_user = models.ForeignKey(User, on_delete=models.CASCADE,related_name='sender')
	to_user = models.ForeignKey(User, on_delete=models.CASCADE,related_name='receiver')
	message = models.TextField(default="")
	message_type = models.TextField(default="TEXT")
	status = models.IntegerField(default=0)# 1= seen,0=unseen
	created_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.message