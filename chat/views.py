from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from friends.models import Friendship,Profile
from django.shortcuts import render
import time
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.db.models import Q
import os
from django.conf import settings
from django.http import JsonResponse
import json
from django.contrib.auth.models import User
from .models import Message
from django.core.serializers.json import DjangoJSONEncoder

# Create your views here.
@login_required
def chat_list(request):
	friend_list=Friendship.objects.filter(user_1=request.user)
	return render(request,'chat_list.html',{'friend_list':friend_list})
@login_required
def conversation(request,user_id):
	user_2=User.objects.get(id=user_id)
	if has_friendship(request.user.id,user_id):


		messages=Message.objects.filter((Q(from_user_id=request.user.id) | Q(to_user_id=user_id))|Q(from_user_id=user_id) | Q(to_user_id=request.user.id)).order_by('id')
		for message in messages:
			message.status=1
			print(message.message)
			message.save()
		return render(request,'chat.html',{'user_2':user_2,'messages':messages})
	else:
		pass

def has_friendship(user_id_1,user_id_2):
	# print(user_id_1)
	# print(user_id_2)
	friendsip = Friendship.objects.filter(user_1_id=user_id_1,user_2_id=user_id_2)
	print(friendsip.count())
	if friendsip.count()>0:
		return True
	else:
		return False

@login_required
@csrf_exempt
def send_msg(request,receiver_id):
	if request.method=='POST':
		msg =json.loads(request.body)
		newMsg=Message(from_user_id=request.user.id,to_user_id=receiver_id,message=msg['msg'],message_type="TEXT")
		newMsg.save()
		if newMsg.pk:
			return HttpResponse(json.dumps({"status": '200'}),content_type='application/json')
		else:
			return HttpResponse(json.dumps({"status": '404'}),content_type='application/json')
	else:
		pass
@login_required
@csrf_exempt
def check_new_msg(request):
	if request.method=='POST':
		newMsgs=Message.objects.filter(to_user_id=request.user.id,status=0).order_by('id')
		tempMesg=list()
		for message in newMsgs:
			message.status=1
			message.save()
			tempMesg.append({'msg':message.message,'username':message.from_user.username,'img':message.from_user.profile.profile_pic.url})
		return HttpResponse(json.dumps({"status": '200','messages':tempMesg},sort_keys=True,indent=1,cls=DjangoJSONEncoder),content_type='application/json')
	else:
		return HttpResponse(json.dumps({"status": '505'}),content_type='application/json')
		